const streamifier = require('streamifier');
const fetch = require('isomorphic-fetch');

module.exports = function (store, folderId, file, callback) {
  if (!file) {
    callback("Not a file", null)
  }
  let newFileName = `${file.originalname}`;
  var fileMetadata = {
    'name': [newFileName],
    parents: [folderId]
  };
  var media = {
    mimeType: 'image/jpeg',
    body: streamifier.createReadStream(file.buffer)
  };
  store.files.create({
    resource: fileMetadata,
    media: media,
    fields: '*'
  }, function (err, data) {
    if (err) {
      callback(err)
    }
    data = data.data;
    var imageLink = "https://drive.google.com/uc?export=view&id=[DATA-ID]";
    imageLink = imageLink.replace("[DATA-ID]",data.id)
    var endpoint = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyCiiQC4mO4X2V3RTVn3con2doZYL6kfZt4";
    var playLoad = {
      "dynamicLinkInfo": {
        "domainUriPrefix": "https://jsdb.page.link",
        "link": imageLink,
      }
    }
    fetch(endpoint, {
      method:"POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(playLoad)
    })
      .then(res => res.json())
      .then(json => {
        var fileResponse = {
          imageUrl: json.shortLink,
          id: data.id,
          name: data.name,
          createdTime: data.createdTime,
          file: {
            type: "jsonDbFile",
            fileType: data.fileExtension,
            size: data.size,
          },
          mediaMeta: data.imageMediaMetadata
        }
        callback(fileResponse)
      })
      .catch(e => callback(e.message))
  });
}
