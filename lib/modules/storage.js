const {google} = require('googleapis');
const g_refresh = "1/nPpZmIKm19Dq5EgN0gKf-92yBs7lAJNtG6VWoPPamDY";
const g_refresh_url = "https://www.googleapis.com/oauth2/v4/token";
const g_client = "1098703716058-opl8e8as0g31n4c7qhjf5gfh8ckkclom.apps.googleusercontent.com";
const g_secret = "QBvgmivgp-c45iRQEgl633iE";
const req = require('request');
const post_body = `grant_type=refresh_token&client_id=${encodeURIComponent(g_client)}&client_secret=${encodeURIComponent(g_secret)}&refresh_token=${encodeURIComponent(g_refresh)}`;

module.exports = function(callback) {
  var result;
  var access_request = {
    url: g_refresh_url,
    body: post_body,
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
  }
  req.post(access_request, function(e, r, b) {
    b = JSON.parse(b);
    if (b.access_token) {
      var OAuth2 = google.auth.OAuth2;
      var oauth2Client = new OAuth2();
      oauth2Client.setCredentials({access_token: b.access_token});
      const drive = google.drive({
        version: 'v3',
        auth: oauth2Client
      });
      callback(drive);
    }else {
      callback(e)
    }
  })
}
