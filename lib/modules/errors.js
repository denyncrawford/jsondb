module.exports = {

  notFound: function(req) {
    var result;
    if (req.accepts("text/html")) {
      result = "<pre>"+JSON.stringify({"message": "Not Found","status": 404}, null, 2)+"</pre>"
    }else if (req.accepts("application/json")) {
      result = {"message": "Not Found","status": 404}
    }
    return result;
  },
  notAuth: function(req) {
    var result;
    if (req.accepts("text/html")) {
      result = "<pre>"+JSON.stringify({"message": "Not authorized to access the resource.","status": 403}, null, 2)+"</pre>"
    }else if (req.accepts("application/json")) {
      result = {"message": "Not authorized to access the resource.","status": 403}
    }
    return result;
  }

}
