const db = require('../db/db.js');
const resolver = require('./resolve.js');
const error = require('./errors.js');

module.exports = {

  authcheck: function(req, res, done) {
    db.users.find({apiKey: req.header("apiKey")}, function(err, doc) {
      if (err) {
        console.log(err);
      }
      if (doc.length === 0) {
        if (req.isAuthenticated()) {
            done();
        }else {
          req.user = {_json: {id:"public"}};
          done();
        }
      }
      if (doc.length !== 0) {
        if (req.header("apiKey") == doc[0].apiKey) {
          // TEMP: id willbe change by gitHubId
          req.user = {_json: {id:doc[0].githubId}};
          done();
        }else {
          req.user = {_json: {id:"public"}};
          done();
          }
        }
    });
  },
  permissions: function(req, res, done) {
    var path = req.originalUrl;
    path = resolver.pathCheck(path);
    var ownsership = false;
    var owner;
    var dbName = req.params[0];
    dbName = resolver.queryCheck(dbName);
    db.stores.find({"data.dbName": dbName}, function(err, doc) {
      if (err) {
        res.send(err);
      }
      if (doc[0] === undefined) {
        res.status(404).send(error.notFound(req));
        return
      }
      if (req.user._json.id == doc[0].data.owner) {
        ownsership = true;
      }
      if (ownsership === true) {
        done();
      }else{
        var dbOwner = doc[0].data.owner;
        db.users.find({githubId: dbOwner}, function(err, user) {
          if (err) {
            res.send(err);
          }
          var publicUrls = user[0].public
          var publicPath = resolver.arrayFind(publicUrls, path);
          if (publicPath !== undefined) {
            done();
          }else {
            res.status(404).send(error.notFound(req));
          }
        });
      }
    });
  }
}
