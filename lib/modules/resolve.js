exports.pathTo = function(obj, path){
    path = path.split('.');
    var current = obj;
    while(path.length) {
        if(typeof current !== 'object') return null;
        current = current[path.shift()];
    }
    if (current == undefined) {
      current = null;
    }
    return current;
}

exports.pathCheck = function(path) {
  var pathLength = path.length; //
  var lastChar = path.charAt(pathLength - 1);
  if (lastChar == ".") {
    path = path.slice(0, pathLength-1);
  }
  if (lastChar == "/") {
    path = path.slice(0, pathLength-1);
  }
  return path;
}

exports.queryCheck = function(param) {
  param = param.split("/");
  param = param[0];
  var paramLength = param.length; //
  var lastChar = param.charAt(paramLength - 1);
  if (lastChar == "/") {
    param = param.slice(0, paramLength-1);
  }
  return param;
}

exports.arrayFind = function (array, value){
	for (var i = 0; i<array.length; i++) {
	   if (array[i] == value){
  	    return array[i]
     }
   }
}

exports.responses = function (req, data) {
  if (req.accepts("text/html")) {
    data = "<pre>"+JSON.stringify(data, null, 2)+"</pre>";
    return data;
  }else if (req.accepts("application/json")) {
    return data;
  }
}
