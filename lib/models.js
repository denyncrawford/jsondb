
exports.dataStore = function(profile) {
  var model = {
    data: {
        owner: profile.id,
        dbName: profile.login
    },
    [profile.login]: {
        getStarted: "See the DOC, and try making a request. Ex.: fetch('https://jsondb.ml/me',{method:'GET',headers:{apiKey:"+profile.apiKey+",accept: 'application/json'})",
        urls: [
          "https://jsondb.ml/_/help",
          "https://jsondb.ml/_/me",
          "https://jsondb.ml/_/logout"
        ]
    }
  }
  return model;
}

exports.dataUsers = function(profile, date) {
  var model = {
      email: profile.email,
      githubId: profile.id,
      username: profile.login,
      requests: {
        "GET": 0,
        "POST": 0,
        "PATCH": 0,
        "DELETE": 0
      },
      accountType: {
          plan: "free"
      },
      created: date,
      updated: date,
      storage:{
        files: {
          folder: null,
          count: 0,
          remaining: 200000
        },
        jsondb: {
          remaining: 1000
        }
      },
      public: [],
      apiKey: profile.apiKey
  };
  return model;
}
