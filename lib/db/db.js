const DataStore = require('nedb');
exports.stores = new DataStore({filename: "./lib/db/stores.json", autoload: true});
exports.users = new DataStore({filename: "./lib/db/users.json", autoload: true});
