var appRoot = require('app-root-path');
const jsonpatch = require('json-merge-patch');
const db = require('../db/db.js');
const resolver = require('../modules/resolve.js');
const googleAccess = require('../modules/storage.js');
const upload = require('../modules/fileUpload.js');
const error = require('../modules/errors.js');
const Notation = require('notation');
const pjson = require(appRoot+'/package.json');
const Multer = require('multer');
const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
  }
});

module.exports = {

  permsPost: function(req, res){
    var database;
    var path = req.originalUrl;
    path = resolver.pathCheck(path);
    path = path.replace("/_/perms", "");
    var username = req.params[0];
    username = resolver.queryCheck(username);
    db.users.find({username: username}, function(err, user) {
      if (err) {
        res.send(err)
      }
      if (user.length === 0) {
        res.send("Not found")
      }else if (user.length !== 0) {
        user = user[0];
        var exitsts = resolver.arrayFind(user.public, path)
        if (exitsts === undefined) {
          user.public.push(path);
          db.users.update({username: username}, {$set: {public:user.public}},{}, function(err, changes) {
            if (err) {
              res.send(err);
            }
            res.send("The path "+path+" is public now.");
          })
        }else {
          res.send("The path "+path+" is already public, if you want to make it private then use DELETE to this endpoint.");
        }
      }
    });
  },
  permsDel: function(req, res) {
    var database;
    var path = req.originalUrl;
    path = resolver.pathCheck(path);
    path = path.replace("/_/perms", "");
    var username = req.params[0];
    username = resolver.queryCheck(username);
    db.users.find({username: username}, function(err, user) {
      if (err) {
        res.send(err)
      }
      if (user.length === 0) {
        res.send("Not found")
      }else if (user.length !== 0) {
        user = user[0];
        var exitsts = resolver.arrayFind(user.public, path)
        if (exitsts === undefined) {
          res.send("The path "+path+" is already private, if you want to make it public then use POST to this endpoint.");
        }else {
          var itemIndex = user.public.indexOf(path);
          delete user.public[itemIndex];
          db.users.update({username: username}, {$set: {public:user.public}},{}, function(err, changes) {
            if (err) {
              res.send(err);
            }
            res.send("The path "+path+" is private now.");
          });
        }
      }
    });
  },
  permsGet: function(req, res) {
    var database;
    var path = req.originalUrl;
    path = resolver.pathCheck(path);
    path = path.replace("/_/perms", "");
    var username = req.params[0];
    username = resolver.queryCheck(username);
    db.users.find({username: username}, function(err, user) {
      if (err) {
        res.send(err)
      }
      if (user.length === 0) {
        res.send("Not found")
      }else if (user.length !== 0) {
        user = user[0];
        var exitsts = resolver.arrayFind(user.public, path)
        if (exitsts === undefined) {
          res.send(resolver.responses(req, {"status":"private"}));
        }else {
          res.send(resolver.responses(req, {"status":"public"}));
        }
      }
    });
  },
  _me: function(req, res) {
    var githubId = req.user._json.id;
    db.users.find({githubId: githubId}, function(err, doc) {
      if (err) {
        res.send(err)
      }
      if (doc.length !== 0) {
        doc = doc[0];
        if (doc.githubId === githubId) {
          res.send(resolver.responses(req, doc));
        }else {
          res.status(404).send(error.notFound(req));
        }
      }else {
        res.status(403).send(error.notAuth(req));
      }
    });
  },
  _meAll: function(req, res) {
    var githubId = req.user._json.id;
    var path = req.originalUrl;
    path = path.replace("/_/me", "");
    path = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    path = resolver.pathCheck(path);
    db.users.find({githubId: githubId}, function(err, doc) {
      if (err) {
        res.send(err)
      }
      if (doc.length !== 0) {
        doc = doc[0];
        var objectGet = new Notation(doc);
        res.send(resolver.responses(req, objectGet.get(path)));
      }else {
        res.status(403).send(error.notAuth(req));
      }
    });
  },
  _version: function (req, res) {
    res.send(resolver.responses(req, pjson.version));
  },
  _help: function (req, res) {
    res.redirect("/");
  },
  _docs: function (req, res) {
    res.render("docs");
  },
  _imgPost: function(req, res) {
    var path = req.originalUrl;
    path = resolver.pathCheck(path);
    path = path.replace("/_/img", "");
    var pathToDot = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    var username = req.params[0];
    username = resolver.queryCheck(username);
    db.users.find({username: username}, function(err, doc) {
      if (err) {
        res.send(err)
      }
      if (doc.length !== 0) {
        doc = doc[0];
        if (doc.storage.files.folder === null) {
          var file = req.file;
          if (file) {
            googleAccess(function(store) {
              var folderId = '1U5UmU7V1Zket2yi3oiXXaEqj1CIeTD6K';
              var fileMetadata = {
                'name': username,
                parents: [folderId],
                'mimeType': 'application/vnd.google-apps.folder'
              };
              store.files.create({
                resource: fileMetadata,
                fields: 'id'
              }, function(err, folder) {
                if (err) {
                  console.error(err);
                }else {
                  db.users.update({username: username}, {$set:{"storage.files.folder": folder.data.id}}, {}, function(err, changes) {
                    if (err) {
                      res.send(err)
                    }
                    upload(store, folder.data.id, file, function(data){
                      if (!data.id) {
                        res.send(data);
                      }
                      db.stores.find({"data.dbName": username}, function(err, doc) {
                        if (err) {
                          res.send(err)
                        }
                        if (doc.length != 0) {
                          doc = doc[0];
                          var getObject = new Notation(doc);
                          var newdoc = getObject.get(pathToDot);
                          if (Array.isArray(newdoc)) {
                            newdoc.push(data)
                          }else {
                            if (Array.isArray(newdoc.img)) {
                              newdoc.img.push(data);
                            }else {
                              var objecData = {[data.id]:data};
                              newdoc = jsonpatch.apply(newdoc, objecData)
                            }
                          }
                          db.stores.update({"data.dbName": username}, {$set:{[pathToDot]:newdoc}}, {}, function(err, change) {
                            if (err) {
                              res.send(err);
                            }
                            res.send(data)
                          })
                        }
                      });
                    });
                  });
                }
              });
            });
          }else {
            res.send("Please send a valid file.")
          }
        }else {
          db.users.find({username: username}, function(err, doc) {
            if (err) {
              res.send(err);
            }
            var file = req.file;
            if (file) {
              doc = doc[0];
              var folder = doc.storage.files.folder;
              googleAccess(function(store) {
                upload(store, folder, file, function(data){
                  if (!data.id) {
                    res.send(data);
                  }
                  db.stores.find({"data.dbName": username}, function(err, doc) {
                    if (err) {
                      res.send(err)
                    }
                    if (doc.length != 0) {
                      doc = doc[0];
                      var getObject = new Notation(doc);
                      var newdoc = getObject.get(pathToDot);
                      if (Array.isArray(newdoc)) {
                        newdoc.push(data)
                      }else {
                        if (!newdoc.img) {
                          newdoc.img = [];
                          newdoc.img.push(data);
                        }else {
                          if (Array.isArray(newdoc.img)) {
                            newdoc.img.push(data);
                          }else {
                            var objecData = {[data.id]:data};
                            newdoc.img = jsonpatch.apply(newdoc.img, objecData)
                          }
                        }
                      }
                      db.stores.update({"data.dbName": username}, {$set:{[pathToDot]:newdoc}}, {}, function(err, change) {
                        if (err) {
                          res.send(err);
                        }
                        res.send(data)
                      })
                    }
                  });
                });
              });
            }else {
              res.send("Please send a valid file.")
            }
          });
        }
      }else {
        res.send("not found")
      }
    });
  }

}
