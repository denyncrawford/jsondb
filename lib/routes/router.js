const express = require('express');
const api = require('./api.js');
const ends = require('./endpoints.js');
const security = require('../modules/sec');
const router = express.Router();
const Multer = require('multer');

module.exports = router;

const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb.
  }
});

//API CONFIG ENDPOINTS

router.get("/*/_/perms", security.authcheck, security.permissions, ends.permsGet);
router.post("/*/_/perms", security.authcheck, security.permissions, ends.permsPost);
router.delete("/*/_/perms", security.authcheck, security.permissions, ends.permsDel);
router.post("/*/_/img", multer.single('file'), security.authcheck, security.permissions, ends._imgPost);
router.get("/_/me", security.authcheck, ends._me);
router.get("/_/me/*", security.authcheck, ends._meAll);
router.get("/_/version", ends._version);
router.get("/_/help", ends._help);

//API CRUD ROUTES ENDPOINTS

router.get("/*", security.authcheck, security.permissions, api.get);
router.post("/*", security.authcheck, security.permissions, api.post);
router.patch("/*", security.authcheck, security.permissions, api.patch);
router.delete("/*", security.authcheck, security.permissions, api.delete);
