const db = require('../db/db.js');
const resolver = require('../modules/resolve.js');
const Notation = require('notation');
const jsonpatch = require('json-merge-patch');

module.exports = {

  get: function(req, res) {
    var dbName = req.params[0];
    var database;
    var path = req.originalUrl;
    path = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    path = resolver.pathCheck(path);
    dbName = resolver.queryCheck(dbName);
    db.stores.find({"data.dbName":dbName}, function(err, doc) {
      if (err) {
        console.log(err);
      }
      database = doc[0];
      var result = resolver.pathTo(database, path);
      if (req.accepts("text/html")) {
        res.send("<pre>"+JSON.stringify(result, null, 2)+"</pre>");
      }else if (req.accepts("application/json")) {
        res.send(result);
      }
    });
  },
  post: function(req, res) {
    var dbName = req.params[0];
    var data = req.body;
    var database;
    var path = req.originalUrl;
    path = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    path = resolver.pathCheck(path);
    dbName = resolver.queryCheck(dbName);
    db.stores.find({"data.dbName": dbName}, function(err, doc) {
      if (err) {
        res.send(err)
      }
      if (doc.length === 0) {
        res.send("Access denied");
      }else if (doc.length !== 0) {
        doc = doc[0];
        var objectAdd = new Notation(doc);
        objectAdd.set(path, data);
        db.stores.update({"data.dbName": dbName}, {$set: {[dbName]: objectAdd.value[dbName]}}, {}, function(err,changes) {
          if (err) {
            res.send(err);
          }
          res.send(data)
        })
      }
    })
  },
  patch: function(req, res, next){
    var dbName = req.params[0];
    var data = req.body;
    var database;
    var path = req.originalUrl;
    var pathToDot = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    path = resolver.pathCheck(path);
    pathToDot = resolver.pathCheck(pathToDot);
    console.log(pathToDot);
    dbName = resolver.queryCheck(dbName);
    db.stores.find({"data.dbName":dbName}, function(err, doc) {
      if (err) {
        res.send(err);
      }
      if (doc.length === 0) {
        res.send("Access denied");
      }else if (doc.length !== 0) {
        doc = doc[0]
        var objectEdit = new Notation(doc);
        var newDoc = objectEdit.get(pathToDot)
        var target = jsonpatch.apply(newDoc, data);
        objectEdit.set(pathToDot, target);
        db.stores.update({"data.dbName": dbName}, {$set:{[dbName]: objectEdit.value[dbName]}}, {}, function(err, changes) {
          if (err) {
            res.send(err);
          }
          var objecGet = new Notation(newDoc);
          res.send(objectEdit.get(pathToDot));
        });
      }
    });
  },
  delete: function(req, res) {
    var dbName = req.params[0];
    var data = req.body;
    ;
    var path = req.originalUrl;
    path = path
    .slice(1)
    .replace(/[\/|\\]/g, ".");
    path = resolver.pathCheck(path);
    dbName = resolver.queryCheck(dbName);
    db.stores.find({"data.dbName":dbName}, function(err, doc){
      if (err) {
        res.send(err);
      }
      if (doc.length === 0) {
        res.send("Access denied");
      }else if (doc.length !== 0) {
        doc = doc[0];
        var objecDelete = new Notation(doc);
        objecDelete.remove(path);
        db.stores.update({"data.dbName": dbName},{$set:{[dbName]:objecDelete.value[dbName]}}, {}, function(err, changes) {
          if (err) {
            res.send(err);
          }
          res.send("Path deleted.")
        });
      }
    });
  }

}
