//Packages

const express = require('express');
const app = express();
const pug = require('pug');
const favicon = require('serve-favicon');
const db = require('./lib/db/db.js');
const resolver = require('./lib/modules/resolve.js');
const models = require('./lib/models.js');
const router = require('./lib/routes/router.js');
const pjson = require('./package.json');
const cors = require('cors');
const uuidv1 = require('uuid/v1');
const moment = require('moment');
const passport = require('passport');
const GitHubStrategy = require('passport-github').Strategy;
db.stores.persistence.setAutocompactionInterval( 7200000 /*ms*/ );
db.users.persistence.setAutocompactionInterval( 7200000 /*ms*/ );

//Passport strategies

passport.use(new GitHubStrategy({
    clientID: "e988fc45f169312ac578",
    clientSecret: "dabf11766665b156df34f09ed0e39366f6a92c96",
    callbackURL: "http://127.0.0.1:8080/auth/github/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    var cbProfile = profile;
    db.users.find({ githubId: Number(profile.id) }, function (err, user) {
      if (err) {
        console.log(err);
        return cb(err);
      }
      if (user.length === 0) {
        profile = profile._json;
        profile.apiKey = uuidv1();
        var currentDate = moment().format();
        db.users.insert(models.dataUsers(profile, currentDate), function(err, user) {
          if (err) {
            console.log(err);
            return cb(err);
          }
          db.stores.insert(models.dataStore(profile), function(err, user) {
            if (err) {
              console.log(err);
            }
            return cb(null, cbProfile);
          })
        });
      }
      if (user.length !== 0) {
        return cb(null, cbProfile);
      }
    });
  }
));

// App config

app.use(express.static(__dirname + "/public"));
app.set("view engine", "pug");
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(cors());
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('body-parser').json());
app.use(require('express-session')({ secret: 'dfgdFDGDfgsFGdFgrfgFG', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

//Auth

app.get('/auth/github',
  passport.authenticate('github'));

app.get('/auth/github/callback',
  passport.authenticate('github', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

app.get('/logout', function(req, res){
  console.log('logging out');
  req.logout();
  res.redirect('/');
});

//Main pages

app.get("/", function(req, res) {
  if (req.user) {
    res.render("index", {
      title: "JsonDB - A simple personal JSON Store as a RESTful API Service.",
      name: req.user.username,
      version: pjson.version
    });
  }else {
    res.render("index", {
      title: "JsonDB - A simple personal JSON Store as a RESTful API Service.",
      version: pjson.version
    });
  }
});

app.get("/_/docs", (req, res) => {
  res.render("docs");
})

//Api requests

app.use('/', router);

//App PORT

app.listen(process.env.PORT || 8080);
