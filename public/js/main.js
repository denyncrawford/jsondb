// codeSnipet 1

var codeSwitch = 0;
codeSelector()
var codeModel;
var instance = new TypeIt('#autotype', {
    strings: ['https://api.jsondb.ml/exampleuser/','https://api.jsondb.ml/exampleuser/db/','https://api.jsondb.ml/exampleuser/db/img/1/'],
    speed: 50,
    breakLines: false,
    autoStart: false,
    startDelete: true,
    nextStringDelay: 5000,
    afterString: function(step, queue, instance) {
      codeSwitch++;
      codeSelector()
    },
    afterComplete: function(step, queue, instance) {
      codeSwitch = 0;
      codeSelector()
    }
});

function codeSelector() {
  if (codeSwitch == 1) {
    codeModel = {
      db: {
        img: [
          "https://jsdb.page.link/img1.png",
          "https://jsdb.page.link/img2.png",
          "https://jsdb.page.link/img3.png"
        ],
        id: 23445634,
        users: {
          "John Doe": true,
          "Carol Crawford": false,
          "PJ": null
        }
      }
    }
    var codeModel = JSON.stringify(codeModel, null, 2);
    document.getElementById("preCode").innerHTML = syntaxHighlight(codeModel);
    document.getElementById("preCode").style.padding = "0 20px 20px 20px";
  }else if (codeSwitch == 2) {
    codeModel = {
      img: [
        "https://jsdb.page.link/img1.png",
        "https://jsdb.page.link/img2.png",
        "https://jsdb.page.link/img3.png"
      ]
    }
    var codeModel = JSON.stringify(codeModel, null, 2);
    $(".spinner").css({ "opacity": 0 , "visibility": "hidden"});
    $(".pic1").css({ "opacity": 1 , "margin-top": "0px", "visibility": "visible"});
    setTimeout(function() {
      $(".pic2").css({ "opacity": 1 , "margin-top": "0px", "visibility": "visible"});
    },200)
    setTimeout(function() {
      $(".pic3").css({ "opacity": 1 , "margin-top": "0px", "visibility": "visible"});
    },300)
    document.getElementById("preCode").innerHTML = syntaxHighlight(codeModel);
  }else if (codeSwitch == 3) {
    codeModel = '<span class="string">"https://jsdb.page.link/img2.png"<span>'
    $(".pic1").css({ "opacity": 0 , "margin-top": "80px", "visibility": "hidden"});
    $(".pic3").css({ "opacity": 0 , "margin-top": "80px", "visibility": "hidden"});
    $(".pic2").css({"transform":"scale(1.5)"});
    document.getElementById("preCode").innerHTML = codeModel;
  }
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

// Start Building

$(".exampleButton").click(function(){
  var id = $(this).attr("rel");
  $(".exampleButton").not(this).removeClass("active");
  $(this).addClass("active");
  $(".codeShow").not("#"+id).fadeOut(200);
  $(".cResult").not("#"+id+"Result").fadeOut(200);
  setTimeout(function() {
    $("#"+id).fadeIn(200);
    $("#"+id+"Result").fadeIn(200);
  },200)
});
