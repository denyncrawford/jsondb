- Getting started

  - [{jsondb}](/)
  - [Quick start](quickstart.md)

- API

  - [Configuration](config.md)
