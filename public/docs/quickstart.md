# Quick Start

This is a quick guide to start using Graviton in a simple way, if you want to learn more, check out how you can use Graviton to the fullest in this guide!

# Installation

Installing graviton is simple, we make it easy for you by giving you our pre-packaged installer so you can get Graviton in your operating system in the most comfortable way possible.

However we also offer our versions in the most popular package installers among the developers.

***

### Windows/Linux/MacOS

1. Download Graviton Installer for your **OS** from the [Last Realeses](https://github.com/Graviton-Code-Editor/Graviton-App/releases).
2. Accept the terms and conditions and choose your installation folder.
3. Open Graviton and **Start Coding!**.

***

### npm

1. **Open your console and type**:

        npm i -g graviton-editor

2. **Execute Graviton**:
 - **Run**:

          graviton init

    > It will open Graviton.

    *OR*

 - **Run**:

          graviton desktop

    > It will open Graviton and add a app icon shortcut into the desktop folder.

3. **Start Codding!**

***

### Chocolatey

1. **Open your console and type**:

        C:\> choco install graviton-Editor

2. Open your installation folder and copy a **Graviton shortcut** into the desktop folder.
3. Open Graviton and **Start Coding!**.

***

### Want another method?

Graviton is Open Source, you can always download the source code and run it manually!

See all the available versions [here](https://github.com/Graviton-Code-Editor/Graviton-App/releases).

***

# Basic Usage

> Usage...
