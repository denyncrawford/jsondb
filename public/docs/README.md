# Graviton

[![HitCount](http://hits.dwyl.io/https://github.com/marc2332/https://github.com/Graviton-Code-Editor/Graviton-App.svg)](http://hits.dwyl.io/https://github.com/marc2332/https://github.com/Graviton-Code-Editor/Graviton-App)

> A lightweight and powerful code editor

Graviton is an easy, lightweight, powerful and free code editor, made by developers for developers.

At Graviton we know that the needs of a developer are fragmented, so we offer you a completely customizable and improvable editor according to your needs, in which you can participate to grow the experience, creating and developing pieces of code that conform to the demand of the community.

![Example](https://github.com/Graviton-Code-Editor/Graviton-App/raw/master/example.jpg)

[Learn how to improve Graviton](#developers)

## Features?

- Right now Graviton counts with a theme customization system. You can build your owns if you want!
- You can change the font-size of your editor.
- Tree view directory, you can manipulate and see what is inside your folders and files.
- Thanks to CodeMirrorJS Graviton has syntax highlighting for a lot of languages, but right now it only supports HTML, CSS and JavaScript.

> We will add more features in the future!

## How it's made?

Graviton is a node.js/Electron.js app, coded on HTML CSS & JavaScript, made to be easy, light and free!.

## Contact & Support

- Create a [GitHub issue](https://github.com/Graviton-Code-Editor/Graviton-App/issues/new) for bug reports, feature requests, or questions
- Follow [@mkenzo_8](https://twitter.com/mkenzo_8) for announcements
- Add a ⭐️ [star on GitHub](https://github.com/Graviton-Code-Editor/Graviton-App/) or ❤️ tweet to support the project!

## License

This project is licensed under the [MIT license](https://github.com/Graviton-Code-Editor/Graviton-App/blob/master/LICENSE.md).

Copyright © [Graviton Code Editor](/)
